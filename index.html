<!DOCTYPE html>
<!-- Copyright notes are just below the head and before body -->

    <html lang="en-US">

        <!-- HTML Header -->
        <head>
            <!-- Title of the page. -->
            <title>Maneage -- Managing data lineage</title>

            <!-- Enable UTF-8 encoding to easily use non-ASCII charactes -->
            <meta charset="UTF-8">
            <meta http-equiv="Content-type" content="text/html; charset=UTF-8">

            <!-- Put logo beside the address bar -->
            <link rel="shortcut icon" href="./img/maneage-logo.svg" />

            <!-- The viewport meta tag is placed mainly for mobile browsers
                that are pre-configured in different ways (for example setting the
                different widths for the page than the actual width of the device,
                or zooming to different values. Without this the CSS media
                solutions might not work properly on all mobile browsers.-->
                <meta name="viewport"
                      content="width=device-width, initial-scale=1">

                <!-- Basic styles -->
                <link rel="stylesheet" href="css/base.css" />
        </head>


        <!--
            Webpage of Maneage: a framework for managing data lineage

            Copyright (C) 2020, Pedram Ashofteh Ardakani <pedramardakani@pm.me>
            Copyright (C) 2020, Mohammad Akhlaghi <mohammad@akhlaghi.org>

            This file is part of Maneage. Maneage is free software: you can
            redistribute it and/or modify it under the terms of the GNU General
            Public License as published by the Free Software Foundation, either
            version 3 of the License, or (at your option) any later version.

            Maneage is distributed in the hope that it will be useful, but
            WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
            General Public License for more details. See
            <http://www.gnu.org/licenses/>.  -->


        <!-- Start the main body. -->
        <body>
            <div id="container">
                <header role="banner">
                    <!-- global navigation -->
                    <nav role="navigation" id="nav-hamburger-wrapper">
                        <input type="checkbox" id="nav-hamburger-input"/>
                        <label for="nav-hamburger-input">|||</label>
                        <div id="nav-hamburger-items" class="button">
                            <a href="index.html">Home</a>
                            <a href="about.html">About</a>
                            <a href="http://git.maneage.org/project.git/">Git</a>
                            <a href="tutorial.html">Tutorial</a>
                        </div>
                    </nav>
                </header>

                <section>
                    <!-- Maneage logo -->
                    <div class="banner">
                        <div>
                          <img src="img/maneage-logo.svg" />
                        </div>
                        <div>
                            <h1>Maneage</h1>
                            <h2>Managing Data Lineage</h2>
                        </div>
                    </div>
                    <p class="clear">Maneage is a framework for having full control over a project's data lineage (thus producing a <a href="http://akhlaghi.org/reproducible-science.html">reproducible result</a>).
		      Maneage is a recipient of the <a href="https://www.rd-alliance.org/node/64603">RDA Europe Adoption grant</a>.
		      To learn more about its founding criteria and a basic introduction, see Akhlaghi et al. (2020, <a href="https://arxiv.org/abs/2006.03018">arXiv:2006.03018</a>), or watch the short talk linked below.
                    </p>

		    <img class="center" src="img/project-flow.svg" width="85%" />

		    <h3>Short video introduction to Maneage:</h3>
		    <p class="clean">By clicking on the image below, you will be taken to the YouTube (in a new tab) to see an invited talk at RDA Adoption week 2020 to introduce Maneage, the <a href="pdf/slides-intro.pdf">slides</a> are also available.</p>
		    <a href="https://www.youtube.com/watch?v=O4bdRu1_Yb0" target="_blank"><img class="center" src="img/rda-talk-on-youtube.jpg" width="60%" /></a>
                </section>

                <section>
                    <h3>Start building your project in Maneage</h3>
                    <p>To start a new project, simply run these commands to clone it from its <a href="http://git.maneage.org/project.git">Git repository</a>.
                    <pre><code>git clone https://git.maneage.org/project.git     <span class="comment"># Clone Maneage, default branch `maneage'.</span>
mv project my-project && cd my-project            <span class="comment"># Set custom name and enter directory.</span>
git remote rename origin origin-maneage           <span class="comment"># Rename remote server to use `origin' later.</span>
git checkout -b master                            <span class="comment"># Make new `master' branch, start customizing.</span></code></pre>
                    </p>
                    <p>You are now ready to configure and make the raw template with the commands below.
                    If they are successful, you can start customizing it.
                    <pre><code>./project configure    <span class="comment"># Build all necessary software from source.</span>
./project make         <span class="comment"># Do the analysis (download data, run software on data, build PDF).</span></code></pre>
                    </p>
                    <p>See the <a href="https://gitlab.com/maneage/project/-/blob/maneage/README-hacking.md#customization-checklist">Customization Checklist</a> in the cloned <code>README-hacking.md</code> file for the next steps to start customizing Maneage for your project.
                    </p>
		    <h3>Submitting bugs and suggesting new features</h3>
		    <p>Development discussions (like list of existing <a href="https://savannah.nongnu.org/bugs/?group=reproduce">bugs</a> and <a href="https://savannah.nongnu.org/task/?group=reproduce">tasks</a>) are currently maintained in <a href="https://savannah.nongnu.org/projects/reproduce/">GNU Savannah</a>.
		      You can <a href="https://savannah.nongnu.org/account/register.php">register in GNU Savannah</a> to <a href="https://savannah.nongnu.org/bugs/?func=additem&group=reproduce">submit a bug</a> or <a href="https://savannah.nongnu.org/task/?func=additem&group=reproduce">submit a task</a>, or comment on an existing bug or task. If you want to submit a general issue without registering on Savannah, you can <a href="https://savannah.nongnu.org/support/?func=additem&group=reproduce">submit an item</a>.</p>
                    <h3>Merge/Pull requests</h3>
                    <p>As you continue customizing Maneage for your own project, you will notice  generic improvements that can be useful for other projects too.
                    In such cases, please send us those changes to implement in the core Maneage branch and let them propagate to all projects using it.
                    If you look through the history of the Maneage branch, you'll notice many users have already started doing this, and this is how Maneage is planned to grow.
                    The recommended process is very similar to
                    <a href="https://www.gnu.org/software/gnuastro/manual/html_node/Forking-tutorial.html">this forking tutorial</a>.
                    Here is a summary:
                    </p>
                    <ol>
                        <li>Go to the <code>maneage</code> branch and create a new branch from there like below:
                            <pre><code>git checkout maneage
git branch -b my-fix</code></pre>
                        </li>
                        <li>Commit your fix over this new branch.</li>
                        <li>Build a new project on your favorite Git repository (GitLab, BitBucket, or GitHub for example) and assign it to a new Git remote in your project.
                            Let's call it <code>my-remote</code>.
                            You only need to do this once and keep this for future fixes.
                        </li>
                        <li>Push your branch to that remote:
                            <pre><code>git push my-remote my-fix</code></pre>
                        </li>
                        <li>Submit a link to your fork and the corresponding branch <a href="http://savannah.nongnu.org/support/?func=additem&group=reproduce">on Savannah</a>.
                            If you are <a href="https://savannah.nongnu.org/account/register.php">registered on Savannah</a>, you can also submit it as <a href="https://savannah.nongnu.org/bugs/?func=additem&group=reproduce">a bug</a> or <a href="https://savannah.nongnu.org/task/?func=additem&group=reproduce">a task</a>.
                        </li>
                    </ol>
                </section>
                <section>
                    <h3>Supporting organizations:</h3>
                    <div class="support">
                        <div id="logo-rda"><a href="https://cordis.europa.eu/project/id/777388"><img src="./img/rda-europe.png"></a></div>
                        <div><a href="https://www.iac.es/"><img src="./img/iac-compressed.svg"></a></div>
                        <div><a href="https://cordis.europa.eu/project/id/339659"><img src="./img/erc-compressed.svg"></a></div>
                        <div id="logo-mext"><a href="https://www.mext.go.jp/en"><img src="./img/mext-compressed.svg"></a></div>
                    </div>
                </section>
                <footer role="contentinfo">
                    <ul>
                        <li><p>Maneage is currently based in the Instituto de Astrofísica de Canarias (IAC).</p></li>
                        <li><p>Address: IAC, Calle Vía Láctea, s/n, E38205 - La Laguna (Tenerife), Spain.</p></li>
                        <!-- The people page will be added later
                        <li><p>People</p></li>
                        -->
                        <li><p>Contact: with <a href="https://savannah.nongnu.org/support/?func=additem&group=reproduce">this form.</a></p></li>
                        <li><p>Copyright &copy; 2020 Maneage volunteers</p></li>
                        <li><p>All logos are copyrighted by the respective institutions</p></li>
                    </ul>
                </footer>
            </div>
        </body>
    </html>
